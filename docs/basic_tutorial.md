So, you want to make a menu for GZDoom using ZForms. Here's a quick guide on
where to start!

- **Note**: this article is not a comprehensive guide on every part of ZForms.
  Ideally, this would all be tutorialized, but for now this will just teach the
  basics. Once you're done reading it, you should be able to start with the
  examples in [the examples
  repository](https://github.com/Doom2fan/ZFormsExamples).

# Prerequisites

You will need Python 3.6 or above to run the Build Script necessary to get
ZForms working.

# Generating the library

- Get the library from
  [the git repository](https://gitlab.com/Gutawer/gzdoom-zforms).

- Use Python 3 to open BuildScript.py.

- You will be prompted to enter a prefix, which will be used in front of all
  classes in the library. You are encouraged to keep this short - for example,
  a mod named My Cool Mod might have a prefix of MCM. Once you have chosen a
  prefix, type it in.

- You will be prompted to choose whether you would like the class names to
  contain underscores. This is a stylistic choice, and will not affect how the
  library works beyond class names.

- You will be prompted to enter a folder name. This can be longer than the
  prefix, and will be where the library files are written to.

- Once this is done, go inside the folder `Generated/<YOUR FOLDER NAME HERE>/`
  and copy the folder called `<PREFIX>_ZForms` (or `<PREFIX>ZForms`, depending
  on underscore choice) into the root of your mod's archive. ZForms should now
  be ready for your use.

# Using the library

To include the entire library in your mod, you can easily just add `#include
"<PREFIX>(_)ZForms/Include.zsc"` in a ZScript file.

Then, we can start making a menu. Here, we'll make a menu based on the ZScript
`GenericMenu` system, which can be opened via `openmenu <menu name>` in the
console, or via `Menu.SetMenu(<menu name>)`, albeit with the caveat that you
must ensure that only the correct player opens the menu like this with a
`consoleplayer` check, which is beyond the scope of this tutorial.

- **Important Note**: Throughout this tutorial and documentation in general,
  the `<PREFIX>` used in the library is treated as being `ZF_`. To write your
  own code, you must therefore replace `ZF_` with your actual prefix.

The basic starting point of this is the [`ZF_GenericMenu`] class, which must be
inherited from:

```
class MyMenu : ZF_GenericMenu {}
```

Right now this menu will do nothing, of course, since it contains no code.
Let's add some required setup stuff:

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // This must be called to ensure that the menu is set up properly.
        Super.init(parent);
        // ZForms menus work by using a "virtual" resolution which means that
        // you only have to design the menu for this resolution.
        // Here we'll use a typical monitor resolution, but this can be anything,
        // even 320x200.
        let baseRes = (1920, 1080);
        setBaseResolution(baseRes);
    }
}
```

Still though, our menu doesn't contain anything. Let's fix that, using the
simplest element available, a [label](ZF_Label).

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before ...

        // Here we create our first element, a label.
        // We'll put it in roughly the top left corner, albeit 100 (virtual)
        // pixels away so that it doesn't look awkward.
        let label = ZF_Label.create(
            // This argument controls the position - 100 pixels from the corner.
            // Note that coordinates in ZForms start from the top left corner,
            // like GZDoom's Screen API.
            (100, 100),
            // This argument controls the size. We need to scale the text up
            // to make it readable at 1080p, so this box needs to be fairly big.
            (400, 200),
            // The text that will be rendered to the screen.
            text: "Hello from ZForms!!!!",
            // How much bigger than "1 pixel in the font = 1 pixel on screen" we want.
            // We'll make it 5 times larger.
            textScale: 5.0
        );
        // The label needs to actually go somewhere, as just creating it does nothing.
        // We'll put it in the top level frame, which is called `mainFrame`.
        label.pack(mainFrame);
    }
}
```

With any luck, you should now get this:

![A picture of a mostly-empty menu with a label saying "Hello from
ZForms!!!!"](basic_tutorial_menu_just_label.png)

Let's similarly add an [image](ZF_Image) to our menu. We have the option to use
a built in Doom texture for this, so we'll use the classic `STARTAN2`.

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before ...

        // Here we create our second element, an image.
        // We'll put it under the label. STARTAN2 is 128x128 pixels
        // so we'll use that size.
        let image = ZF_Image.create(
            // This argument controls the position - 50 pixels under the label.
            (100, 250),
            // This argument controls the size. Here we match STARTAN2.
            (128, 128),
            // The texture name.
            image: "STARTAN2"
        );
        // As before we pack it into the mainFrame.
        image.pack(mainFrame);
    }
}
```

After this, you should get the following menu:

![A picture of a mostly-empty menu with a label saying "Hello from ZForms!!!!",
and an image](basic_tutorial_menu_label_image.png)

Great! Except, is it really a menu, right now? I mean, there's absolutely no
interactive parts. Let's fix that.

To do that, we'll need to learn a new concept - [box textures](ZF_BoxTextures).
Box textures are like a normal GZDoom texture, except we slice it up into 9
pieces - the corners, the sides, and the middle. Then, when we draw something
like a [button](ZF_Button) which has a background, then instead of scaling the
background texture to match, ZForms will instead draw the corners at the
corners, and either stretch or tile the sides and middle to fit the desired
size.

This results in a much better looking element without scale distortions, and
avoids having to create bespoke image assets for all sizes of buttons.

To create box textures, we add the following code:

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before ...

        // Here we create the box textures. We use a graphic that is in the
        // tutorial later, and we need to pass coordinates of the middle section
        // in so that ZForms knows how to slice it up. The `createTexturePixels`
        // function takes these coordinates in as pixels.
        let background = ZF_BoxTextures.createTexturePixels(
            // The name of the texture.
            "Graphics/CommonBackground.png",
            // Where the top-left corner of the middle part is.
            (7, 7),
            // Where the bottom-right corner of the middle part is.
            (14, 14),
            // Whether we should tile or scale the sides (`true` means scale).
            true,
            // Whether we should tile or scale the middle (`true` means scale).
            true
        );
    }
}
```

This is the texture file used in the above example:

!["A dark 21px by 21px box with rounded corners"](CommonBackground.png)
**Graphics/CommonBackground.png**

Before we make a button, we'll make a simpler element - a [box
image](ZF_BoxImage) element, which is just like an image, except that it uses
box textures:

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before ...

        // Here we create our third element, a box image.
        // We'll put it under the normal image, with the box textures we just created.
        let boxImage = ZF_BoxImage.create(
            // This argument controls the position - we'll put it under the image.
            (100, 400),
            // This argument controls the size.
            (400, 200),
            // The box textures.
            background
        );
        // Again, we pack it into the mainFrame.
        boxImage.pack(mainFrame);
    }
}
```

Now you should have this:

![A picture of a mostly-empty menu with a label saying
"Hello from ZForms!!!!", an image, and a large grey
box with rounded corners](basic_tutorial_menu_box_image.png)

Notice how even though the texture is 21 pixels by 21 pixels, and our box image
is 400 pixels by 200 pixels, we still have crisp edges that haven't been
distorted at all! That's what box textures are useful for.

Now that we've got one box textures working, we'll need to add a few more for a
[button](ZF_Button), as buttons exist in a few states - they're either
unhovered, hovered, or clicked. Here's the code:

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before ...

        let normal = ZF_BoxTextures.createTexturePixels(
            "Graphics/CommonBackgroundNormal.png",
            (7, 7),
            (14, 14),
            true,
            true
        );
        let hover = ZF_BoxTextures.createTexturePixels(
            "Graphics/CommonBackgroundHover.png",
            (7, 7),
            (14, 14),
            true,
            true
        );
        let click = ZF_BoxTextures.createTexturePixels(
            "Graphics/CommonBackgroundClick.png",
            (7, 7),
            (14, 14),
            true,
            true
        );
    }
}
```

Here are the textures you'll need:

!["A dark-ish 21px by 21px box with rounded corners"](CommonBackgroundNormal.png)
**Graphics/CommonBackgroundNormal.png**

!["A grey 21px by 21px box with rounded corners"](CommonBackgroundHover.png)
**Graphics/CommonBackgroundHover.png**

!["A light grey 21px by 21px box with rounded corners"](CommonBackgroundClick.png)
**Graphics/CommonBackgroundClick.png**

Now, let's create a button!

```
class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before ...

        // Here we create our fourth element, a button.
        // We'll put it under the box image, with the box textures we just created.
        let button = ZF_Button.create(
            // This argument controls the position - we'll put it under the box image.
            (100, 650),
            // This argument controls the size.
            (200, 100),
            // What the button will say on top of it.
            text: "Click on me!!",
            // The various box textures for the button states.
            inactive: normal,
            hover: hover,
            click: click
        );
        // Again, we pack it into the mainFrame.
        button.pack(mainFrame);
    }
}
```

Now you should see this:

![A picture of a mostly-empty menu with a label saying
"Hello from ZForms!!!!", an image, a large dark grey
box with rounded corners, and a grey box with rounded
corners with the text "Click on me!!" on it
](basic_tutorial_menu_button.png)

And you'll notice that when mousing over the button, it changes color, and
clicking on it changes the color even more! Still though, it's not very useful.
We have no way of making our code do anything with the fact that it's been
clicked...

Let's fix that. To do this, we have to introduce a new concept -
[handlers](ZF_Handler). If you've ever used GZDoom's event handler system, this
should feel familiar.

We start by making a new class which inherits from [`ZF_Handler`]:

```
class MyMenuHandler : ZF_Handler {}
```

Then, we'll register it to the events we care about by overriding virtual
functions. Currently, we only want button click events, and right now, we'll
just log some stuff rather than doing anything.

```
class MyMenuHandler : ZF_Handler {
    override void buttonClickCommand(ZF_Button caller, Name command) {
        // Tell us which button sent this, and what the `command` is.
        Console.printf("Button %p sent event \"%s\"", caller, command);
    }
}
```

Then, we'll have to set up the handler. This involves actually telling the
button to send events to the handler, so we'll have to modify it:

```
// ... The handler definition ...

class MyMenu : ZF_GenericMenu {
    override void init(Menu parent) {
        // ... All the stuff from before the button ...

        let cmdHandler = new("MyMenuHandler");

        let button = ZF_Button.create(
            (100, 650),
            (200, 100),
            text: "Click on me!!",

            // The command handler this button uses.
            cmdHandler: cmdHandler,
            // The command name it sends when it sends commands.
            command: 'firstButtonClick',

            inactive: normal,
            hover: hover,
            click: click
        );
    }
}
```

Now, the menu won't have visually changed, but you'll notice that if you press
the button, a message gets printed to the console, containing the name of the
command! Which is great, except it's probably not what you'd ever want a menu
to do. Let's modify the handler to affect the menu it's defined for.

We'll make it so that when the button is clicked, it moves the label a bit to
the right every time. To do this, we'll need to let the handler have a
reference to the menu, and we'll need to make the label into a member variable
for the menu so that it can be modified after creation. This will also show an
example of modifying properties of ZForms elements after creation, which is
always done via "getters" and "setters".

```
class MyMenuHandler : ZF_Handler {
    // A reference to the menu we want to modify - the menu has to set this
    // to itself.
    MyMenu menu;

    override void buttonClickCommand(ZF_Button caller, Name command) {
        // Check if the command matches the button's command.
        if (command == 'firstButtonClick') {
            // Use the getter for the X position on the label, and add 50 to it.
            let newPosX = menu.label.getPosX() + 50;
            // Use the setter for the X position on the label to set it to the new value,
            // which is its old value plus 50.
            menu.label.setPosX(newPosX);
        }
    }
}

class MyMenu : ZF_GenericMenu {
    // Store a reference to the label now that we want to change it later.
    ZF_Label label;

    override void init(Menu parent) {
        // ... Stuff from before the label ...

        // Note the change from `let ` to `self.`.
        self.label = ZF_Label.create(
            // ... Label set-up arguments ...
        );
        // Same here.
        self.label.pack(mainFrame);

        // ... Stuff between the label and `cmdHandler` ...

        let cmdHandler = new("MyMenuHandler");
        // Make sure `cmdHandler` knows about this menu, so it can modify it.
        cmdHandler.menu = self;

        // ... Button definition ...
    }
}
```

What you should now see is that rather than a message being printed, the label
now moves when the button is clicked.

Still, it feels really abrupt. It'd be nice if the label slid over to its new
position. To achieve this, we'll need to run some code every tick. We'll
override the menu's `ticker` virtual to achieve this.

The plan here is pretty simple: we'll add a field to the menu called
`desiredPosX` which is where we *want* the label to be. Then, in the ticker,
we'll move the label by a maximum of 5 pixels in the direction needed to get
the label there. That way, the label will move at a rate of 5 pixels per tick
to its new position, instead of jumping instantly!

```
class MyMenuHandler : ZF_Handler {
    // A reference to the menu we want to modify - the menu has to set this
    // to itself.
    MyMenu menu;

    override void buttonClickCommand(ZF_Button caller, Name command) {
        // Check if the command matches the button's command.
        if (command == 'firstButtonClick') {
            // Add 50 to the menu's desired label X position.
            menu.desiredPosX += 50;
        }
    }
}

class MyMenu : ZF_GenericMenu {
    // Store a reference to the label now that we want to change it later.
    ZF_Label label;

    // Where we want the label to end up.
    double desiredPosX;

    override void init(Menu parent) {
        // ... Stuff from before the label ...

        self.label = ZF_Label.create(
            // ... Label set-up arguments ...
        );
        // Set the desired X position to the starting X position so it doesn't
        // instantly move.
        self.desiredPosX = self.label.getPosX();
        self.label.pack(mainFrame);

        // ... Stuff from after the label ...
    }

    override void ticker() {
        // Get the difference between where the label should be, and where it is now.
        let posX = self.label.getPosX();
        let diff = self.desiredPosX - posX;
        // Make sure it doesn't go above 5.0 in either direction.
        diff = clamp(diff, -5.0, 5.0);
        // Move the label that amount this tick.
        let newPosX = posX + diff;
        self.label.setPosX(newPosX);
    }
}
```

Now, when we click the button, the label slides to the right over 10 ticks
rather than instantly jumping. Just as a bit of practice, let's make a tiny
modification so that we can move it back left again, with a new button:

```
class MyMenuHandler : ZF_Handler {
    // A reference to the menu we want to modify - the menu has to set this
    // to itself.
    MyMenu menu;

    override void buttonClickCommand(ZF_Button caller, Name command) {
        // Check if the command matches the button's command.
        if (command == 'firstButtonClick') {
            // Add 50 to the menu's desired label X position.
            menu.desiredPosX += 50;
        } else if (command == 'secondButtonClick') {
            // Subtract 50 from the menu's desired label X position.
            menu.desiredPosX -= 50;
        }
    }
}

class MyMenu : ZF_GenericMenu {
    ZF_Label label;
    double desiredPosX;

    override void init(Menu parent) {
        // ... Stuff from before ...

        let anotherButton = ZF_Button.create(
            (100, 690),
            (250, 50),
            text: "Click on me too!!",

            cmdHandler: cmdHandler,
            // The command name it sends when it sends commands - we've changed this
            // to be `second` instead of `first`!
            command: 'secondButtonClick',

            inactive: normal,
            hover: hover,
            click: click,
            textScale: 2.0
        );
        // Again, we pack it into the mainFrame.
        anotherButton.pack(mainFrame);
    }

    // ... The ticker ...
}
```

You should now see this:

![A picture of a menu with a label saying "Hello from ZForms!!!!", an image, a
large dark grey box with rounded corners, a grey box with rounded corners with
the text "Click on me!!" on it, and another grey box with the text "Click on me
too!!" on it](basic_tutorial_menu_two_buttons.png)

If you click on the top button, the label will move right. If you click on the
bottom button, the label will move left.

Let's make a minor modification to this to demonstrate a new concept -
[frames](ZF_Frame). If a bunch of elements are positioned as a group, it can
make a lot of sense to group them together in code too - that way, all of them
can be moved just by moving the entire group!

This concept is called a [frame](ZF_Frame) in ZForms, and we've actually been
using one the entire time - `mainFrame`. But frames in ZForms can be nested
into other frames, so we'll make it so that pressing the buttons moves the
label *and* image *and* box image, by putting them into a frame.

Note that when we change the code here, we change the position of all the
elements we're putting inside the frame - this is because element coordinates
are *relative to the frame they're in* - so when we previously had the label
at `(100, 100)`, we now need it at `(0, 0)`, because the frame is at `(100,
100)`. We similarly subtract `(100, 100)` from the coordinates of the image
and box image. We then pack them into the variable called `frame` instead
of `mainFrame`:

```
// ... The handler ...

class MyMenu : ZF_GenericMenu {
    ZF_Label label;
    double desiredPosX;

    override void init(Menu parent) {
        // ... Stuff from before the label ...

        // Here we make a frame - it needs to be positioned right, and big enough
        // to contain the elements we want inside it.
        let frame = ZF_Frame.create(
            (100, 100),
            (400, 500)
        );
        frame.pack(mainFrame);

        self.label = ZF_Label.create(
            // This argument controls the position - 0 pixels from the corner
            // *of the frame*, which is 100 pixels from the screen corner.
            (  0,   0),
            (400, 200),
            text: "Hello from ZForms!!!!",
            textScale: 5.0
        );
        self.desiredPosX = self.label.getPosX();
        // We now put this in `frame`!
        label.pack(frame);

        let image = ZF_Image.create(
            (  0, 150),
            (128, 128),
            image: "STARTAN2"
        );
        // As before we pack it into `frame`.
        image.pack(frame);

        let background = ZF_BoxTextures.createTexturePixels(
            // ... createTexturePixels arguments ...
        );

        let boxImage = ZF_BoxImage.create(
            (  0, 300),
            (400, 200),
            background
        );
        // Again, we pack it into `frame`.
        boxImage.pack(frame);

        // ... Stuff after the boxImage creation ...
    }

    // ... Ticker ...
}
```

Note that we haven't updated the ticking code yet, so this won't yet move the
group. This has been done on purpose to demonstrate something - if you now
press the buttons, you'll now notice that the label starts becoming clipped:

![A picture of a menu with a cut off label reading as "-lo from -rms!!!!", an
image, a large dark grey box with rounded corners, a grey box with rounded
corners with the text "Click on me!!" on it, and another grey box with the text
"Click on me too!!" on it](basic_tutorial_menu_label_clipped.png)

This is because elements in ZForms - with very few exceptions - *do not draw
outside of their own boundaries*. Because the label is in the frame, it cannot
be drawn outside of the frame.

Ok, let's actually do what we intended to - we just need to change the `label`
member variable to be the frame instead, and stop the label creation code from
trying to set it or `desiredPosX`.

```
class MyMenu : ZF_GenericMenu {
    // Store a reference to the frame now that we want to change it later.
    ZF_Frame frame;

    double desiredPosX;

    override void init(Menu parent) {
        // ... Stuff from before the frame ...

        // We now set the member variable here.
        self.frame = ZF_Frame.create(
            (100, 100),
            (400, 500)
        );
        self.desiredPosX = self.frame.getPosX();
        frame.pack(mainFrame);

        let label = ZF_Label.create(
            (  0,   0),
            (400, 200),
            text: "Hello from ZForms!!!!",
            textScale: 5.0
        );
        // Note the lack of setting `desiredPosX`.
        label.pack(frame);

        // ... Stuff after the label creation ...
    }

    override void ticker() {
        // Get the difference between where the frame should be, and where it is now.
        let posX = self.frame.getPosX();
        let diff = self.desiredPosX - posX;
        // Make sure it doesn't go above 5.0 in either direction.
        diff = clamp(diff, -5.0, 5.0);
        // Move the frame that amount this tick.
        let newPosX = posX + diff;
        self.frame.setPosX(newPosX);
    }
}
```

This code should illustrate why frames are so helpful - without it, we'd need a
separate desired position variable for each element we wanted to move! When
designing a menu you'll want to group up things that are logically "grouped"
into frames.

This is the end of the basic tutorial - congratulations! You can find the full
code [here](FullBasicTutorialCode.zsc).

# Where to go from here

ZForms has a lot more API to cover than just this. Notably, there's a bunch of
elements I didn't even start to talk about here - [toggle
buttons](ZF_ToggleButton), [radio buttons](ZF_RadioButton),
[sliders](ZF_Slider), [text inputs](ZF_TextInput), [dropdown
lists](ZF_DropdownList), and an [element that makes another one
scrollable](ZF_ScrollContainer), and more!

Additional API examples can be seen in [the examples
repo](https://github.com/Doom2fan/ZFormsExamples), as mentioned above.

This website also serves as API documentation - if you'd like to learn about an
element, search for it at the top! As time goes on more examples will be
provided, both in the repo and inline in the documentation.

If you can't work out how something works, people who know ZForms (including
the main author, Gutawer) are around on the [ZDoom
Discord](https://forum.zdoom.org/viewtopic.php?t=54921) who can answer
questions about it - tagging @Gutawer is a good way to get answers if you're
truly stuck!

There's a few things you might be already curious about, like how a menu is
supposed to actually affect the game world. To do this, you'd use GZDoom's
`SendNetworkEvent` system, which is beyond the scope of the ZForms
documentation as it's actually a general use system for affecting the world
from the UI.
