class MyMenuHandler : ZF_Handler {
    // A reference to the menu we want to modify - the menu has to set this
    // to itself.
    MyMenu menu;

    override void buttonClickCommand(ZF_Button caller, Name command) {
        // Check if the command matches the button's command.
        if (command == 'firstButtonClick') {
            // Add 50 to the menu's desired label X position.
            menu.desiredPosX += 50;
        } else if (command == 'secondButtonClick') {
            // Subtract 50 from the menu's desired label X position.
            menu.desiredPosX -= 50;
        }
    }
}

class MyMenu : ZF_GenericMenu {
    // Store a reference to the frame now that we want to change it later.
    ZF_Frame frame;

    // Where we want the frame to end up.
    double desiredPosX;

    override void init(Menu parent) {
        // This must be called to ensure that the menu is set up properly.
        Super.init(parent);
        // ZForms menus work by using a "virtual" resolution which means that
        // you only have to design the menu for this resolution.
        // Here we'll use a typical monitor resolution, but this can be anything,
        // even 320x200.
        let baseRes = (1920, 1080);
        setBaseResolution(baseRes);

        // Here we make a frame - it needs to be positioned right, and big enough
        // to contain the elements we want inside it.
        self.frame = ZF_Frame.create(
            (100, 100),
            (400, 500)
        );
        // Set the desired X position to the starting X position so it doesn't
        // instantly move.
        self.desiredPosX = self.frame.getPosX();
        frame.pack(mainFrame);

        // Here we create our first element, a label.
        // We'll put it in roughly the top left corner, albeit 100 (virtual)
        // pixels away so that it doesn't look awkward.
        let label = ZF_Label.create(
            // This argument controls the position - 0 pixels from the corner
            // *of the frame*, which is 100 pixels from the screen corner.
            // Note that coordinates in ZForms start from the top left corner,
            // like GZDoom's Screen API.
            (  0,   0),
            // This argument controls the size. We need to scale the text up
            // to make it readable at 1080p, so this box needs to be fairly big.
            (400, 200),
            // The text that will be rendered to the screen.
            text: "Hello from ZForms!!!!",
            // How much bigger than "1 pixel in the font = 1 pixel on screen" we want.
            // We'll make it 5 times larger.
            textScale: 5.0
        );
        // We put this in `frame`.
        label.pack(frame);

        // Here we create our second element, an image.
        // We'll put it under the label. STARTAN2 is 128x128 pixels
        // so we'll use that size.
        let image = ZF_Image.create(
            // This argument controls the position - 50 pixels under the label.
            (  0, 150),
            // This argument controls the size. Here we match STARTAN2.
            (128, 128),
            // The texture name.
            image: "STARTAN2"
        );
        // As before we pack it into `frame`.
        image.pack(frame);

        // Here we create the box textures. We use a graphic that is in the
        // tutorial later, and we need to pass coordinates of the middle
        // section in so that ZForms knows how to slice it up.
        let background = ZF_BoxTextures.createTexturePixels(
            // The name of the texture.
            "Graphics/CommonBackground.png",
            // Where the top-left corner of the middle part is.
            (7, 7),
            // Where the bottom-right corner of the middle part is.
            (14, 14),
            // Whether we should tile or scale the sides (`true` means scale).
            true,
            // Whether we should tile or scale the middle (`true` means scale).
            true
        );

        // Here we create our third element, a box image. We'll put it under
        // the normal image, with the box textures we just created.
        let boxImage = ZF_BoxImage.create(
            // This argument controls the position - we'll put it under the image.
            (  0, 300),
            // This argument controls the size.
            (400, 200),
            // The box textures.
            background
        );
        // Again, we pack it into `frame`.
        boxImage.pack(frame);

        let normal = ZF_BoxTextures.createTexturePixels(
            "Graphics/CommonBackgroundNormal.png",
            (7, 7),
            (14, 14),
            true,
            true
        );
        let hover = ZF_BoxTextures.createTexturePixels(
            "Graphics/CommonBackgroundHover.png",
            (7, 7),
            (14, 14),
            true,
            true
        );
        let click = ZF_BoxTextures.createTexturePixels(
            "Graphics/CommonBackgroundClick.png",
            (7, 7),
            (14, 14),
            true,
            true
        );

        let cmdHandler = new("MyMenuHandler");
        // Make sure `cmdHandler` knows about this menu, so it can modify it.
        cmdHandler.menu = self;

        // Here we create our fourth element, a button.
        // We'll put it under the box image, with the box textures we just created.
        let button = ZF_Button.create(
            // This argument controls the position - we'll put it under the box image.
            (100, 620),
            // This argument controls the size.
            (200, 50),
            // What the button will say on top of it.
            text: "Click on me!!",

            // The command handler this button uses.
            cmdHandler: cmdHandler,
            // The command name it sends when it sends commands.
            command: 'firstButtonClick',

            // The various box textures for the button states.
            inactive: normal,
            hover: hover,
            click: click,
            textScale: 2.0
        );
        // Again, we pack it into the mainFrame.
        button.pack(mainFrame);

        let anotherButton = ZF_Button.create(
            (100, 690),
            (250, 50),
            text: "Click on me too!!",

            cmdHandler: cmdHandler,
            // The command name it sends when it sends commands - we've changed this
            // to be `second` instead of `first`!
            command: 'secondButtonClick',

            inactive: normal,
            hover: hover,
            click: click,
            textScale: 2.0
        );
        // Again, we pack it into the mainFrame.
        anotherButton.pack(mainFrame);
    }

    override void ticker() {
        // Get the difference between where the frame should be, and where it
        // is now.
        let posX = self.frame.getPosX();
        let diff = self.desiredPosX - posX;
        // Make sure it doesn't go above 5.0 in either direction.
        diff = clamp(diff, -5.0, 5.0);
        // Move the frame that amount this tick.
        let newPosX = posX + diff;
        self.frame.setPosX(newPosX);
    }
}
