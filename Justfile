document_copy:
    @rm -rf ./doc_generated/src/
    @mkdir -p ./doc_generated/src/
    @cp -r ./docs/   ./doc_generated/src/
    @cp -r ./ZForms/ ./doc_generated/src/
    @find ./doc_generated/src/ -type f -exec sed -i -e 's/####/ZF_/g' {} \;
    @find ./doc_generated/src/ -type f -exec sed -i -e 's/##PATH##/ZForms/g' {} \;

document: document_copy
    @mkdir -p ./doc_generated/docs/
    zscdoc -f ./doc_generated/src/ -o ./doc_generated/docs/gzdoom-zforms --delete-without-confirm

default_coverage := "breakdown"
document_coverage cov=default_coverage: document_copy
    zscdoc -f ./doc_generated/src/ --coverage={{cov}}
