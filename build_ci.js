import fs from "fs/promises";
import path from "path";

const mkdir = async (dirPath) => {
    try {
        await fs.mkdir(dirPath, { recursive: true });
    } catch (err) {
        if (err.code !== "EEXIST") throw err;
    }
};

const copyRecursive = async (src, dest) => {
    const entries = await fs.readdir(src, { withFileTypes: true });
    for (const entry of entries) {
        const srcPath = path.join(src, entry.name);
        const destPath = path.join(dest, entry.name);

        if (entry.isDirectory()) {
            await mkdir(destPath);
            await copyRecursive(srcPath, destPath);
        } else {
            await fs.copyFile(srcPath, destPath);
        }
    }
};

const replaceInFiles = async (dir, replacements) => {
    const files = await fs.readdir(dir, { withFileTypes: true });

    for (const file of files) {
        const filePath = path.join(dir, file.name);

        if (file.isDirectory()) {
            await replaceInFiles(filePath, replacements);
        } else {
            let content = await fs.readFile(filePath, "utf-8");
            for (const [searchValue, replaceValue] of replacements) {
                content = content.replaceAll(searchValue, replaceValue);
            }
            await fs.writeFile(filePath, content);
        }
    }
};

const generateZForms = async (out, prefix) => {
    const zFormsSrcPath = "./ZForms";
    const docsSrcPath = "./docs";
    const zFormsDestPath = path.join(out, `${prefix}ZForms`);
    const docsDestPath = path.join(out, "docs");

    await mkdir(out);

    await mkdir(zFormsDestPath);
    await copyRecursive(zFormsSrcPath, zFormsDestPath);
    await mkdir(docsDestPath);
    await copyRecursive(docsSrcPath, docsDestPath);
    await replaceInFiles(zFormsDestPath, [
        ["####", `${prefix}ZF_`],
        ["##PATH##", `${prefix}ZForms`],
    ]);
};

await generateZForms("doc_generated/src", "");
console.log("Generated successfully.");
